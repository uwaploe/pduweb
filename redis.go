package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/gomodule/redigo/redis"
)

// Redis pub-sub channels
const (
	PduCommand  = "pdu.command"
	PduRawData  = "data.pdu.*"
	PduHcbData  = "data.pdu.HCB.*"
	PduProcData = "proc.pdu.*"
)

type device string

func (d device) cmdChannel() string {
	return string(d) + ".command"
}

func (d device) rawChannels() string {
	return "data." + string(d) + ".*"
}

func (d device) hcbChannels() string {
	return "data." + string(d) + ".HCB.*"
}

func (d device) procChannels() string {
	return "proc." + string(d) + ".*"
}

// Pass all received Redis pub-sub messages to a Go channel.
func pubsubReader(psc redis.PubSubConn) <-chan redis.Message {
	c := make(chan redis.Message, 1)
	go func() {
		defer close(c)
		for {
			switch msg := psc.Receive().(type) {
			case error:
				log.Println(msg)
				return
			case redis.Subscription:
				if msg.Count == 0 {
					log.Println("Pubsub channel closed")
					return
				} else {
					log.Printf("Subscribed to %q", msg.Channel)
				}
			case redis.Message:
				select {
				case c <- msg:
				default:
				}
			}
		}
	}()

	return c
}

func sendCommand(conn redis.Conn, w http.ResponseWriter, d device, cmd string) {
	n, err := redis.Int(conn.Do("PUBLISH", d.cmdChannel(), []byte(cmd)))
	w.Header().Set("Content-Type", "text/plain")
	if err != nil {
		w.WriteHeader(500)
		fmt.Fprintf(w, "Redis error: %v", err)
	} else {
		fmt.Fprintf(w, "Command sent to %d clients\n", n)
	}
}

func newPool(server string) *redis.Pool {
	return &redis.Pool{
		MaxIdle:     3,
		MaxActive:   16,
		IdleTimeout: 120 * time.Second,
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", server)
		},
		TestOnBorrow: func(c redis.Conn, t time.Time) error {
			if time.Since(t) < time.Minute {
				return nil
			}
			_, err := c.Do("PING")
			return err
		},
	}
}
