module bitbucket.org/uwaploe/pduweb

go 1.13

require (
	bitbucket.org/mfkenney/go-sse v0.6.0
	github.com/coreos/go-systemd/v22 v22.0.0
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/gorilla/mux v1.7.3
)
