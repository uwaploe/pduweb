// Pduweb provides an HTTP interface to monitor data from and send
// commands to the inVADER PDU or PFU.
//
// Data records are provided as a Server Sent Events stream by sending
// an HTTP GET request to the "/stream" URL. Commands are sent by an
// HTTP POST request to "/cmd", the body of the request should contain
// a single parameter named "text" whose value is the NMEA command to
// send (without the leading "$"), for example:
//
//  curl -X POST -d "text=PWREN,CCD,1,BEXP,1" http://10.0.97.214:8080/cmd
//

package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"strings"
	"syscall"
	"time"

	sse "bitbucket.org/mfkenney/go-sse"
	"github.com/coreos/go-systemd/v22/activation"
	"github.com/gomodule/redigo/redis"
	"github.com/gorilla/mux"
)

const Usage = `Usage: pduweb [options] devname

Provide an HTTP server to interface with the inVADER PDU. Devname is either
"pdu" or "pfu".
`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	rdAddr  string = "localhost:6379"
	svcAddr string = "127.0.0.1:8080"
)

func lookupEnvOrString(key string, defaultVal string) string {
	if val, ok := os.LookupEnv(key); ok {
		return val
	}
	return defaultVal
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&rdAddr, "rdaddr",
		lookupEnvOrString("REDIS_SERVER", rdAddr),
		"Redis server host:port")
	flag.StringVar(&svcAddr, "addr",
		lookupEnvOrString("PDUWEB_ADDR", svcAddr),
		"HTTP server host:port")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()
	if len(args) < 1 {
		flag.Usage()
		os.Exit(1)
	}
	devName := device(strings.ToLower(args[0]))

	// Drop log timestamps when running under Systemd
	if _, ok := os.LookupEnv("JOURNAL_STREAM"); ok {
		log.SetFlags(0)
	}

	// Redis connection for the pub-sub subscription
	rdconn, err := redis.Dial("tcp", rdAddr)
	if err != nil {
		log.Fatalf("Cannot connect to Redis: %v", err)
	}
	defer rdconn.Close()

	// Redis connection pool for commands
	pool := newPool(rdAddr)

	broker := sse.NewBroker()
	r := mux.NewRouter()
	r.Handle("/stream", broker)
	r.HandleFunc("/cmd", func(w http.ResponseWriter, req *http.Request) {
		conn := pool.Get()
		defer conn.Close()
		sendCommand(conn, w, devName, req.FormValue("text"))
	}).Methods("POST")

	psc := redis.PubSubConn{Conn: rdconn}
	ch := pubsubReader(psc)
	psc.PSubscribe(devName.rawChannels())
	psc.PSubscribe(devName.procChannels())
	psc.Subscribe(devName.cmdChannel())

	svc := &http.Server{
		Handler:     r,
		ReadTimeout: 15 * time.Second,
	}

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	// Goroutine to wait for signals or for the signal channel
	// to close.
	go func() {
		s, more := <-sigs
		if more {
			log.Printf("Got signal: %v", s)
			psc.PUnsubscribe()
			psc.Unsubscribe()
			svc.Shutdown(context.Background())
		}
	}()

	listeners, err := activation.Listeners()
	if err != nil {
		log.Fatalf("Systemd activation error: %v", err)
	}

	var listener net.Listener
	switch n := len(listeners); n {
	case 0:
		// Not activated by a Systemd socket ...
		addr, _ := net.ResolveTCPAddr("tcp", svcAddr)
		listener, err = net.ListenTCP("tcp", addr)
		if err != nil {
			log.Fatal(err)
		}
	case 1:
		listener = listeners[0]
	default:
		log.Fatalf("Unexpected number of socket activation fds: %d", n)
	}

	// Start HTTP server
	go func() {
		svc.Serve(listener)
	}()

	for msg := range ch {
		broker.Notify(sse.Event{Name: msg.Channel, Data: msg.Data})
	}
}
