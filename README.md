# Web Interface for PDU Monitoring

Pduweb provides a simple HTTP interface to monitor data from, and send commands to, the inVADER PDU or PFU. The application listens for connections on TCP ports 8080 (pdu) and 8081 (pfu). Direct connections are only allowed from the local system. The [Nginx](https://www.nginx.com) HTTP server provides authenticated reverse-proxy access for remote systems.

## Data Monitoring

Data records are streamed from the server using [server-sent events](https://en.wikipedia.org/wiki/Server-sent_events) push technology. The endpoint URI is `/pdu/stream` or `/pfu/stream`.

``` shellsession
$ curl -u USER:PASSWORD -X GET http://10.0.97.214/pdu/stream
event: data.pdu.FGPIO
data: {"time":"2020-01-21T19:06:54.717253109Z","source":"FGPIO","data":null}

event: data.pdu.ADC1R
data: {"time":"2020-01-21T19:06:54.754990659Z","source":"ADC1R","data":{"scan":[228,58578,53004,5108,25796,4138,5,40]}}

event: data.pdu.FGPIO
data: {"time":"2020-01-21T19:06:55.687706552Z","source":"FGPIO","data":null}

event: data.pdu.ADC5R
data: {"time":"2020-01-21T19:06:55.727704521Z","source":"ADC5R","data":{"scan":[58429,34612,25835,52984,844,805,711,784]}}
^C
$
```

Each event name has the form *data.NAME.TAG*, where *NAME* is the device (pdu or pfu) and *TAG* is the tag name of the NMEA sentence which supplied the data. The event data is a JSON object containing the parsed sentence data along with a timestamp and the sentence tag.

## Commands

Commands are sent using the HTTP POST method to the URI `/pdu/cmd` or `/pfu/cmd`. The command is supplied as the value of the single parameter, *text*. Valid commands are the NMEA sentences defined in the PDU/PFU Interface Control Documents without the leading `$` or trailing checksum.

``` shellsession
$ curl -u USER:PASSWORD -X POST -d "text=PWREN,BEXP,1" http://10.0.97.214/pdu/cmd
Command sent to 1 clients
$
```

## Nginx Proxy Configuration

``` nginx
location /pdu/ {
            auth_basic "Pdu realm";
            auth_basic_user_file /etc/nginx/conf.d/nginx.htpasswd;

            proxy_pass http://127.0.0.1:8080/;
            proxy_buffering off;
            proxy_http_version 1.1;
            proxy_set_header Connection "";
            proxy_set_header Host $host;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header  X-Real-IP         $remote_addr;
        }
location /pfu/ {
            auth_basic "Pdu realm";
            auth_basic_user_file /etc/nginx/conf.d/nginx.htpasswd;

            proxy_pass http://127.0.0.1:8081/;
            proxy_buffering off;
            proxy_http_version 1.1;
            proxy_set_header Connection "";
            proxy_set_header Host $host;
            proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
            proxy_set_header  X-Real-IP         $remote_addr;
        }
```
